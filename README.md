[![Build Status](https://travis-ci.org/pry/pry-media.svg?branch=master)](https://travis-ci.org/pry/pry-media)

Mirrors:

* [GitHub](https://github.com/r-obert/pry-media)
* [GitLab](https://gitlab.com/r-obert/pry-media)

#### pry-media

pry-media is a plugin for the [Pry repl](https://github.com/pry/pry).
The plugin adds the option to play media items through Pry with an external media
player. The media player chosen by default varies from platform to platform, although
it is configurable and it can be changed to what is optimal for your personal preferences.

Despite the title and all descriptions up until this point making reference to "media" in the
same sense as a music video or film, the plugin is also suitable for audio books.

#### The default playlist

pry-media ships with a large default playlist with topics ranging from music to religion. It is
not intended to cause offense, but offer food for the brain and soul. It might not be your cup of
tea. It covers Islam, which in todays climate is a touch-y topic given how widespread Islamophobia
has become.

For this reason and because Pry always offers choice, there is an --import option
which can import a custom playlist instead, effectively replacing the default until you choose a
new playlist or revert to the default. Keep the doors of your mind open, please.

The default list is maintained as a
[gist](https://gist.github.com/r-obert/3630ab501137b288ef74dbab8e8d36bd)
and distributed through [pastebin](https://pastebin.com/raw/0p62kpDW) that will come into use
when pry-media reaches v1.0.0 and used henceforth to add new media.

#### Usage

_Note: "hey-dj" command is also aliased as recall-memory, hey-tiesto, hey-armin-van-buuren, and
hey-piano-man._

1. Start with a summary of the help menu:

        [1] pry(main)> hey-dj --help

2. Play media items in sequence, repeating the sequence when the end is reached:

        [1] pry(main)> hey-dj-seq

        # Alternative to the above:
        [1] pry(main)> hey-dj --sequential

3. Play a random media item:

        [1] pry(main)> hey-dj

4. Find and play a media item through a pattern match:

		[1] pry(main)> hey-dj --match "Gift Of Love"

5. Find and print all media items that match a pattern:

		[1] pry(main)> hey-dj --find "Love"
	    ....

6. The --import option can be used to fetch the latest changes to the default playlist.

		[1] pry(main)> hey-dj --import default
		Please wait while playlist is fetched across the wire …
		[2] pry(main)>

7. It can also be used to import your own playlist.

		[1] pry(main)> hey-dj --import "https://www.mydomain.com/myplaylist.yml"
		Please wait while playlist is fetched across the wire …
		[2] pry(main)>

8. Local disk playlists are supported through the `file:` scheme, too.

		[1] pry(main)> hey-dj --import "file:/home/myname/myplaylist.yml"
		Please wait while playlist is fetched across the wire …
		[2] pry(main)>

For more information on the `--import` option, see the [doc/](doc/)
directory. For more information while using Pry, refer to step 1.

#### Alternative playlists

pry-media ships with its own alternative playlists, but you can add your own in the format
described [here](doc/form_paper.md) to "~/.pry/media/user-playlists/" and they will be picked
by the "hey-dj --alternative" (or "hey-dj -a" for short) command.

__0.__ Africa And Carribean

	recall-memory --import 0

__1.__ Americas

	recall-memory --import 1

__2.__ Barack Hussein Obama

	recall-memory --import 2

__3.__ Christianity

	recall-memory --import 3

__4.__ Europe

	recall-memory --import 4

__5.__ Iran

	recall-memory --import 5

__6.__ Islam

	recall-memory --import 6

__7.__ Islamic Ayatollah

	recall-memory --import 7

__8.__ John Fitzgerald Kennedy (JFK)

    recall-memory --import 8

__9.__ Judaism

	recall-memory --import 9

__10.__ Poems (Rumi)

	recall-memory --import 10

__11.__ Trap Nation

	recall-memory --import 11

__12.__ Tupac Amaru Shakur

	recall-memory --import 12

__13.__ USA

	recall-memory --import 13

#### System Install (Recommended, Easiest method)

If you're using Windows, please install [Ruby](https://rubyinstaller.org) before going further.
Then open powershell.exe (MS Windows) and enter the following:

	gem install pry pry-media --no-rdoc --no-ri
	pry

Or open Terminal.app(Mac OS X), and enter the following:

	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	brew install ruby
	PATH=/usr/local/bin:$PATH gem install pry pry-media --no-rdoc --no-ri
	pry

#### Local Install (Optional, Experience Required)

As long as you have a local install of 'rake', & 'bundler', at a shell:

	git clone https://github.com/pry/pry-media.git
	cd pry-media
	ruby -S rake start

#### License

[MIT](./LICENSE.txt).
