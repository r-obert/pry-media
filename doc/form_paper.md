#### Form paper

A playlist takes the following form: An array composed of other two-element Arrays. The first
element is a URL to a media item, and the second element is a title for the media item. The sample
is an interesting vlog series from Iran, that you can import with the following command:

__Import__

	[1] pry(main)> hey-dj --import "https://pastebin.com/raw/ub1d0gbP"
	[2] pry(main)> hey-dj-seq
	[3] pry(main)> hey-dj-seq
	[4] pry(main)> hey-dj-seq
	[5] pry(main)> hey-dj-seq
	# The end! :(

__Sample__

	---
	- - https://www.youtube.com/watch?v=MqClD2Noiwc
      - IRAN "VLOG" 1
	- - https://www.youtube.com/watch?v=v1K6rLKhz8Y
	  - IRAN "VLOG" 2
	- - https://www.youtube.com/watch?v=vmVHKZWU2pM
	  - IRAN "VLOG" 3
	- - https://www.youtube.com/watch?v=o59Gqevl_4o
	  - IRAN "VLOG" 4
