#### hey-dj --import

The --import paper uses two fictional characters: Alice and Bob. Alice would like to share
a playlist with Bob, and vice versa. Alice found some nice videos about Iran, and tells Bob
over a fictional instant messaging platform:

	Alice: Hey Bob, I made this list for you! Check it out:
	https://pastebin.com/raw/ub1d0gbP

Bob interrupts his Pry session, and imports the URL given to him:

	[10] pry(main)> hey-dj --import https://pastebin.com/raw/ub1d0gbP
	[11] pry(main)> hey-dj-seq # Plays the first video
	[12] pry(main)> hey-dj-seq # Plays the second video

Bob replies:

	Bob: Amazing culture, very interesting. Thanks Alice! I should visit!
	I have a list about Saudi Arabia, check it out: https://www.pryrepl.org/~bob/saudiarabia.yml

Alice interrupts her Pry session, and imports the URL given to her:

    [10] pry(main)> hey-dj --import https://www.pryrepl.org/~alice/saudiarabia.yml
    [11] pry(main)> hey-dj-seq # Plays the first video
    [12] pry(main)> hey-dj-seq # Plays the second video

Alice replies:

	Alice: Mind blown. I should visit!

That's the general idea of the `--import` feature. It does not have to be limited to YouTube
videos. A playlist can be made up of URLs from all around the web, with one requirement:
HTTP**S** only. That's all there is to it. But one more thing. There is also a `--mem` switch
for reading a playlist into process memory instead of using a persistent disk. For example, you
could keep the Iran Vlog series for the next time you start Pry but still switch to the default
playlist while your Pry session is active:

	[1] pry(main)> hey-dj --import default --mem

Alice and Bob can return to the jamming, and of course, to writing Ruby! :-)
