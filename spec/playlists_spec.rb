require_relative "setup"
RSpec.describe "Playlists" do
  shared_examples "alternative playlist" do |name, playlist|
    specify "no duplicate URLs" do
      expect(playlist.map{|m|m[0]}.uniq!).to eq(nil)
    end

    specify "first element is a HTTP URI" do
      playlist.each do |m|
        expect(URI.parse(m[0])).to be_a(URI::HTTP), "#{m[0]} is not a URI (#{name})"
      end
    end
  end

  include_examples "alternative playlist",
                   "default",
                   YAML.load(Net::HTTP.get_response(URI.parse(Pry::Media::Download::DEFAULT)).body)
  include_examples "alternative playlist",
                   "Islam",
                   YAML.load_file("playlists/Islam.yml")
  include_examples "alternative playlist",
                   "Ayatollah Khamenei",
                   YAML.load_file("playlists/Islamic Ayatollah.yml")
  include_examples "alternative playlist",
                   "Europe",
                   YAML.load_file("playlists/Europe.yml")
  include_examples "alternative playlist",
                   "Tupac Amaru Shakur",
                   YAML.load_file("playlists/Tupac Amaru Shakur.yml")
  include_examples "alternative playlist",
                   "Barack Hussein Obama",
                   YAML.load_file("playlists/Barack Hussein Obama.yml")
  include_examples "alternative playlist",
                   "Nabeel Qureshi",
                   YAML.load_file("playlists/Christianity.yml")
  include_examples "alternative playlist",
                   "USA",
                   YAML.load_file("playlists/USA.yml")
  include_examples "alternative playlist",
                   "Poems (Rumi)",
                   YAML.load_file("playlists/Poems (Rumi).yml")
  include_examples "alternative playlist",
                   "Africa and The Carribean",
                   YAML.load_file("playlists/Africa and The Carribean.yml")
  include_examples "alternative playlist",
                   "Iran Vlogs",
                   YAML.load_file("playlists/Iran.yml")
  include_examples "alternative playlist",
                   "Americas",
                   YAML.load_file("playlists/Americas.yml")
end
