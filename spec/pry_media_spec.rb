require_relative 'setup'
RSpec.describe Pry::Media do
  before :each do
    Pry::Testable.set_testenv_variables
    Pry::Media.default_configure
    FileUtils.rm_rf File.dirname(Pry::Media::Download::LOCAL_PATH)
    pry_eval("hey-dj --import default")
  end

  after :each do
    Pry::Testable.unset_testenv_variables
  end

  after :all do
    FileUtils.rm_rf File.dirname(Pry::Media::Download::LOCAL_PATH)
  end

  describe '--count' do
    subject do
      pry_eval "hey-dj --count"
    end

    it 'counts media items' do
      expect(subject).to match(/\d{1,3} media items available/)
    end
  end

  describe '--mem' do
    before :each do
      FileUtils.rm_rf File.dirname(Pry::Media::Download::LOCAL_PATH)
    end

    it "imports playlist into memory" do
      pry_eval "hey-dj --import default --mem"
      expect(pry_eval("hey-dj --count")).to match(/\d{1,3} media items available/)
      expect(File.exist?(Pry::Media::Download::LOCAL_PATH)).to be(false)
    end
  end

  describe '_pry_.config.media.set' do
    let(:media_set) do
      pry_eval("_pry_.config.media.set")
    end

    specify 'Array' do
      expect(media_set).to be_instance_of(Array)
    end

    specify 'composed of Pry::Media::Item objects' do
      media_set.each do |item|
        expect(item).to be_instance_of(Pry::Media::Item)
      end
    end

    specify 'each attribute is a String' do
      media_set.each do |item|
        expect(item.title).to be_instance_of(String)
        expect(item.url).to be_instance_of(String)
      end
    end
  end

  describe 'SSL' do
    specify 'raises SchemeError on HTTP URI' do
      expect {
        pry_eval("hey-dj --import http://www.pryrepl.org")
      }.to raise_error(Pry::Media::Download::SchemeError)
    end
  end

  describe 'HTTP Redirect' do
    specify 'raises HTTPError on redirect', pending: RUBY_ENGINE == "jruby" do
      expect {
        pry_eval("hey-dj --import https://google.com/")
      }.to raise_error(Pry::Media::Download::HTTPError)
    end
  end
end
