require 'bundler/setup'
require 'pry'
require 'pry/testable'
require 'pry-media'
require 'fileutils'
RSpec::Expectations.configuration.on_potential_false_positives = :nothing

RSpec.configure do |config|
  config.include Pry::Testable::Evalable
  config.before :each do |example|
    if example.metadata[:pending] == true
      skip "skip"
    end
  end
end
