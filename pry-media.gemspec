require "./lib/pry-media/version"
Gem::Specification.new do |g|
  g.name = 'pry-media'
  g.homepage = 'https://github.com/pry/pry-media'
  g.authors = ['Robert']
  g.email = ['1xAB@protonmail.com', 'jrmair@gmail.com']
  g.version = Pry::Media::VERSION
  g.summary = 'Pry: My media, your media.'
  g.description = <<-DESCRIPTION
  pry-media is a plugin for the Pry repl.
  The plugin adds the option to play media items through Pry with
  an external media player. The media player chosen by default varies
  from platform to platform, although it is configurable and it can be
  changed to what is optimal for your personal preferences.
  DESCRIPTION
  g.licenses = ['MIT']
  g.required_ruby_version = ">= 2.1"
  g.files = Dir["doc/*", "lib/**/*.rb", "*.{txt,md}", "playlists/*.yml"]
  g.add_runtime_dependency "pry", "~> 0.11.3"
  g.add_development_dependency "bundler", "~> 1.0"
end
