require "bundler/setup"
Bundler.require :development
Gem::Tasks.new

desc "Start pry + pry-media."
task :pry do
  sh "ruby -S bundle install --path .vendored"
  sh "ruby -S bundle exec pry"
end
task start: :pry

desc "Run the specs."
task :spec do
  sh "bundle exec rspec"
end
task default: :spec

desc "Release"
task :__release__ => [:spec, :release]
