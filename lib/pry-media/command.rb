class Pry::Media::PlayMedia < Pry::ClassCommand
  include Pry::Helpers::Text
  include Pry::Media
  include OptionDispatcher
  include Playlistable

  match "hey-dj"
  group "Media"
  description "My media. Your media."
  command_options argument_required: false
  banner <<-BANNER
  hey-dj [OPTIONS]

  With no options given, a random media item is played
  instead.
  BANNER

  def options(o)
    o.on :import,
         'Import the default playlist, or import a new playlist.',
         argument: true
    o.on :mem,
         'Import a playlist into process memory, without using a persistent disk'
    o.on :m, :match,
         'Play the first media item that matches a pattern.',
         argument: true
    o.on :u, :'url-find',
         'Print the first media item that is equal to a URL.',
         argument: true
    o.on :f, :find,
         'Print all media items that match a pattern.',
         argument: true
    o.on :s, :sequential,
         'Play media items in sequential order. Rewinds to the start at the end.',
         argument: false
    o.on :i, :index,
         'Play a media item at a specific index.',
         argument: true
    o.on :c, :count,
         'Print the number of media items available to play.',
         argument: false
    o.on :l, :list,
         'Print a list of available media items.',
         argument: false
    o.on :'-1',
          'Play the last (most recent) media item in the list.'
    o.on :a, :alternative,
         'Print a list of alternative playlists that can be imported from disk.',
         argument: false
    o.on :v, :version,
         'Print the version of pry-media.'
  end

  def process
    if opts.present?(:import)
      storage_medium = opts.present?(:mem) ? :ram_disk : :persistent_disk
    elsif not opts.present?(:alternative) and _pry_.config.media.set.nil?
      playlist_init(pry)
    end
    case
    when opts.present?(:import)      then playlist_create(opts[:import], storage_medium, pry)
    when opts.present?(:match)       then find_all_by_pattern(opts[:match], 0, pry)
    when opts.present?(:find)        then find_all_by_pattern(opts[:find], pry)
    when opts.present?(:'url-find')  then find_by_url(opts[:'url-find'], pry)
    when opts.present?(:sequential)  then sequential(pry)
    when opts.present?(:list)        then print_list(pry)
    when opts.present?(:count)       then print_count(pry)
    when opts.present?(:index)       then at_index(opts[:index], pry)
    when opts.present?(:'-1')        then at_index(-1, pry)
    when opts.present?(:alternative) then print_alternative_playlists(pry)
    when opts.present?(:version)     then pry.pager.page("v#{Pry::Media::VERSION}")
    else play # Play a media item at random.
    end
  end

  private
  def play(item=nil)
    item ||= begin
      require 'securerandom' if not defined?(SecureRandom)
      randint = SecureRandom.random_number(mset.size)
      mset.entries[randint]
    end
    pry.pager.page sprintf("Playing '%s'", item.title)
    if windows? and pry.config.media.player == :WIN32OLE
      require 'WIN32OLE' if not defined?(WIN32OLE)
      explorer = WIN32OLE.new 'Shell.Application'
      explorer.Open item.url
    else
      pry.config.spawn.call(pry, [*pry.config.media.player, item.url])
    end
  end

  def mset
    _pry_.config.media.set
  end

  def mset_seq
    _pry_.config.media.set_enum
  end

  alias_method :pry, :_pry_
  Pry::Commands.add_command(self)
  Pry::Commands.alias_command 'recall-memory', 'hey-dj'
  Pry::Commands.alias_command 'hey-tiesto', 'hey-dj'
  Pry::Commands.alias_command 'hey-armin-van-buuren', 'hey-dj'
  Pry::Commands.alias_command 'hey-piano-man', 'hey-dj'
  Pry::Commands.alias_command 'hey-dj-seq', 'hey-dj --sequential'
end
