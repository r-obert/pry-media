class Pry::Media::PlaylistMap
  #
  # @params [Array<Proc>]
  #   One or more Proc objects who expand to a one or more rows
  #   of '[name, path]' arrays when called.
  #
  def initialize(*globs)
    @h = globs.inject({}) {|m, glob|
      to_h = glob.call.map {|path| [File.basename(path, '.yml'), path] }
      m.merge!(Hash[to_h])
    }
  end

  #
  # @return [Array<String>]
  #   Returns an array of playlist names.
  #
  def names
    @h.keys
  end

  #
  # @return [Array<String>]
  #   Returns an array of playlist paths.
  #
  def paths
    @h.values
  end

  #
  # @param [String] name
  #   Lookup a path based on name of a playlist.
  #
  def [](name)
    @h[name]
  end

  #
  # @param [Integer] offset
  #   The number to starting counting items from.
  #
  # @param [Symbol] color_sym
  #   The color of each number in the list (eg: ':bright_blue').
  #
  # @return [String]
  #   Return the playlist in a nicely formatted list.
  #
  def with_line_numbers(offset, color_sym)
    text_format.with_line_numbers names.join($/), offset, color_sym
  end

  #
  # @param [String] index
  #   An index as a String.
  #
  # @return [String, nil]
  #   Returns the name of the playlist at _index_, or nil.
  #
  def at(index)
    if index[/\A\d+\z/]
      names.find.with_index {|_, i| i == index.to_i }
    end
  end

  private
  def text_format
    Pry::Helpers::Text
  end
end
