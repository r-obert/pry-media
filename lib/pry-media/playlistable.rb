# coding: utf-8
module Pry::Media::Playlistable
  def playlist_init(pry)
    path = Pry::Media::Download::LOCAL_PATH
    if File.exist? path
      playlist_parse(path, :persistent_disk, pry)
    else
      playlist_create(:default, :persistent_disk, pry)
    end
  end

  def playlist_create(uri, storage_medium, pry)
    pry.pager.page green("Please wait while playlist is fetched across the wire …")
    download = Pry::Media::Download.new(uri)
    payload = storage_medium == :persistent_disk ?
              download.store(download.body) :
              download.body
    playlist_parse payload, storage_medium, pry
  end

  private def playlist_parse(payload, storage_medium, pry)
    ary = YAML.safe_load storage_medium == :persistent_disk ?
                         File.read(Pry::Media::Download::LOCAL_PATH) :
                         payload
    pry.config.media.set = ary.map! { |ary| Pry::Media::Item.new(*ary) }
    pry.config.media.set_enum = pry.config.media.set.to_enum
  end
end
