# frozen_string_literal: true
class Pry::Media::Download
  require 'net/https'
  require 'fileutils'
  include Pry::Media

  DEFAULT = 'https://pastebin.com/raw/0p62kpDW'
  LOCAL_PATH = File.join(HOMEPATH, "playlist.yml")
  SUPPORTED_SCHEMES = %w(https file)

  SchemeError = Class.new(RuntimeError)
  HTTPError = Class.new(RuntimeError)

  def initialize(uri)
    @uri = begin
             uri = uri.to_s
             pmap = Pry::Media::PlaylistMap.new(P_GLOB, U_GLOB)
             case
             when byindex = pmap.at(uri) # at 0, 1, 2, etc.
               URI.parse("file:%s" % URI.encode(pmap[byindex]))
             when byname = pmap[uri]
               URI.parse("file:%s" % URI.encode(byname))
             else
               URI.parse(uri.downcase == 'default' ? DEFAULT : uri)
             end
           end
  end

  def body
    if not SUPPORTED_SCHEMES.include? @uri.scheme
      raise SchemeError, "#{@uri.scheme} is not supported."
    end
    if @uri.scheme == "file"
      return read_file_body(@uri)
    end
    client = Net::HTTP.start(@uri.host, @uri.port, use_ssl: true)
    case res = client.get(@uri.path)
    when Net::HTTPOK then res.body
    else raise(HTTPError, "Bad server response (#{res.class})")
    end
  ensure
    client.finish if client
  end

  def store(body)
    FileUtils.mkdir_p(HOMEPATH)
    File.binwrite LOCAL_PATH, body
    LOCAL_PATH
  end

  private def read_file_body(uri)
    if Pry::Platform.windows?
      File.read URI.decode(uri.to_s[/[^file:]+.+/])
    else
      File.read URI.decode(uri.path)
    end
  end
end
