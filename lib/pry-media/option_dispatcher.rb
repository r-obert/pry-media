# coding: utf-8
module Pry::Media::OptionDispatcher
  include Pry::Media
  def at_index(int, pry)
    raise IndexError,
          "Out of bounds (0..#{mset.size-1})" if not int.to_i.between?(-1, mset.size-1)
    play mset.entries[Integer(int)]
  end

  def sequential(pry)
    play mset_seq.next
  rescue StopIteration
    mset_seq.rewind
    retry
  end

  def print_alternative_playlists(pry)
    format_str = "%{head}#{$/}%{body}"
    set = Pry::Media::PlaylistMap.new(P_GLOB, U_GLOB)
    pry.pager.page format_str % {
      head: heading("pry-media alternative playlists"),
      body: set.with_line_numbers(0, :bright_blue),
    }
  end

  def print_list(pry)
    str = mset.map {|item| item.title }.join($/)
    pry.pager.page with_line_numbers(str, 0, :bright_blue)
  end

  def print_count(pry)
    pry.pager.page "#{bold(mset.size)} media items available."
  end

  def find_by_url(url, pry)
    item, index = Pry::Media::ItemQuery.new(mset).find_by_url(url)
    if item
      str = sprintf "%s: %s", bright_blue(index), green(item.title)
      pry.pager.page(str)
    else
      pry.pager.page("No matches :(")
    end
  end

  def find_all_by_pattern(pattern, limit=-1, pry)
    found = Pry::Media::ItemQuery.new(mset).find_all_by_pattern(pattern)
    if found and limit == 0
      play found[0][0]
    elsif found
      str = found[0..limit].map {|(item, index)|
        sprintf "%s: %s", bright_blue(index), green(item.title)
      }.join($/)
      pry.pager.page(str)
    else
      pry.pager.page "No matches :("
    end
  end
end
