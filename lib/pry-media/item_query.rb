class Pry::Media::ItemQuery
  def initialize(mset)
    @mset = mset
  end

  def find_by_url(url)
    @mset.each.with_index do |item, index|
      url.strip == item.url and return [item, index]
    end
    nil
  end

  def find_all_by_pattern(pattern)
    found = []
    @mset.each_with_index do |item, index|
      if pattern_match?(item, pattern)
        found.push [item, index]
      end
    end
    found.empty? ? nil : found
  end

  private
  def pattern_match?(item, pattern)
    item.title =~ /#{pattern}/i
  end
end
