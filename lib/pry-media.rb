# frozen_string_literal: true
require 'pry' if not defined?(Pry)
require 'set' if not defined?(SortedSet)
require 'yaml' if not defined?(YAML)

module Pry::Media
  HOMEPATH = File.join((ENV['HOME'] or ENV['HOMEPATH']), '.pry', 'media')
  U_GLOB = lambda { Dir[File.join(HOMEPATH, "user-playlists", "*.yml")] }
  P_GLOB = lambda { Dir[File.expand_path(File.join(__dir__, "..", "playlists", "*.yml"))] }
  Item = Struct.new(:url, :title)

  require_relative "pry-media/playlist_map"
  require_relative "pry-media/playlistable"
  require_relative "pry-media/download"
  require_relative "pry-media/item_query"
  require_relative "pry-media/option_dispatcher"
  require_relative "pry-media/command"
  require_relative "pry-media/version"

  def self.default_configure
    Pry.configure do |config|
      config.media = Pry::Config.from_hash({}, nil)
      config.media.player = default_player
      config.spawn = lambda do |pry, ary, env={}, options={}|
        Process.wait Kernel.spawn env,
                                  [ary[0], "PryDJ"], *ary[1..-1],
                                  options.merge!({[:out, :err] => pry.output})

      end
    end
  end

  def self.default_player
    if Pry::Platform.mac_osx?
      ['open']
    elsif Pry::Platform.windows?
      # On Windows, we use Shell::Application.open() via
      # WIN32OLE by default, so return :WIN32OLE.
      :WIN32OLE
    else
      ['xdg-open']
    end
  end

  default_configure
  FileUtils.mkdir_p File.join(HOMEPATH, "user-playlists")
end
